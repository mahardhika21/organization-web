@extends('backoffice.layouts.app')
@section('css')
    <link href="{{ asset('storage/assets/backoffice/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="content-wrapper" style="min-height: 218px;">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="info-box">
                        <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text"> Organitation</span>
                            <span class="info-box-number" id="organization">
                                0
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Manager</span>
                            <span class="info-box-number" id="manager">0</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix hidden-md-up"></div>
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text"> Pic</span>
                            <span class="info-box-number" id="pic">0</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

           <div class="col-sm-12">
                    <div class="card card-success">
                        <div class="card-header" style="background-color: #09aaaf">
                            <h3 class="card-title"><i class="fas fa-users"></i>Daftar Organitation</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card">
                                       <form action="{{ route('organization.cari') }}" method="post">
                                        @csrf
                                            <input type="text" placeholder="cari berdasar namaorganization atau nama pic" name="search" style="width:340px">
                                            <button type="submit"><i class="fa fa-search"></i></button>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-body">
                                               @if (session('message_error_data'))
                                                <div class="alert alert-warning">
                                                    {{ session('message_error_data') }}
                                                </div>
                                                @endif
                                          <table id="dataTable" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                     <th>Website</th>
                                                    <th>Logo</th>
                                                </tr>
                                            </thead>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            <!-- /.row -->

            <!-- Main row -->

            <!-- /.row -->
        </div>
        <!--/. container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection

@section('js')
<script src="{{ asset('storage/assets/backoffice/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('storage/assets/backoffice/js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    var base_url = '{{ url("/") }}';
     function getInfo()
    {
        let http = new XMLHttpRequest();
        http.onreadystatechange = function() {
            if(this.readyState == 4 && this.status == 200) {
                let jdata = JSON.parse(this.responseText);
                //alert(jdata.data.klien);
                if(jdata.success == 'true') {
                    document.getElementById('organization').innerHTML = jdata.data.organization;
                    document.getElementById('manager').innerHTML  = jdata.data.manager;
                    document.getElementById('pic').innerHTML = jdata.data.pic;
                }
            }
        }
        http.open('GET', '{{ route("info_bar") }}');
        http.send();
    }
    getInfo();

    var table = $('#dataTable').DataTable({
        serverSide: true,
        processing: true,
        responsive: true,
        autoWidth: false,
        ajax: {
            url  : "{{ route('organization.datatables') }}",
            type : 'GET',
            data : function(d) {
                d.filter_date = $('#daterange').val();
            },
        },
        "drawCallback": function(settings) {
        //create sesi on button
       // OnEditing()
        },
        columns: [
            { name: 'id', searchable: false },
            { name: 'name', searchable: false },
            { name: 'email', searchable: true},
            { name: 'phone', searchable: false},
            { name: 'website', searchable: false},
            { name: 'logo', searchable: false},
        ],
        columnDefs : [
         {
            targets: 0,
            orderable: true,
            render: function ( data, type, row, meta ){
            return meta.row + meta.settings._iDisplayStart + 1;
         }
         },
         {
             targets : 1,
             orderable : true,
             render : function (data, type, row, meta) {
                 return '<a href="'+base_url +'/organization/'+row[0]+'">'+ data +'</a>';
             }
         },
         {
             targets : 2,
             orderable : true,
             render : function (data, type, row, meta) {
                 return '<a href="'+base_url +'/organization/klien/'+row[0]+'">'+ data +'</a>';
             }
         },
          {
             targets : 3,
             orderable : true,
             render : function (data, type, row, meta) {
                 return '<a href="'+base_url +'/organization/klien/'+row[0]+'">'+ data +'</a>';
             }
         },
          {
             targets : 5,
             orderable : true,
             render : function (data, type, row, meta) {
                 if( data == "" || data == null) {
                    return "<img src='https://via.placeholder.com/150'>";
                    } else {
                    return "<img src='"+data+"' width='150'>";
                }
             }
         }
        ],
    });
</script>
@endsection
