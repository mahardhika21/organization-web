<form action="{{ route('users.destroy', $user->id) }}" method="post">
    @csrf
    @method('DELETE')
    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-secondary btn-sm edit-data">
        Edit
    </a>
    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Hapus data ini ?')">Hapus</button>
</form>