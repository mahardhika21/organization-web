@extends('backoffice.layouts.app')
@section('css')

@endsection
@section('content')
<div class="content-wrapper" style="min-height: 180px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $title }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Manager</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card  card-primary">
                    <div class="card-header" style="background-color: #09aaaf">
                        <h3 class="card-title">{{ $title }}</h3>
                    </div>
                      @if (session('message_success'))
                            <div class="alert alert-success">
                                {{ session('message_success') }}
                            </div>
                    @endif
                    @if($errors->any())
                        {{ implode('', $errors->all('<div>:message</div>')) }}
                    @endif
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div class="card-body">
                        @if (session('message_success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ session('message_success') }}
                        </div>
                        @endif
                        <form method="POST" action="{{ $action == 'create' ? route('organization.store') : route('organization.update', $organization->id) }}"
                            enctype="multipart/form-data">
                            @csrf
                            @if($action == 'edit')
                            @method('PUT')
                            @endif
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputNama">Nama</label>
                                        <input type="hidden" value="{{ $action  }}" name="action" />
                                        <input type="hidden" value="{{ $menu  }}" name="role"/>
                                        <input type="text" class="form-control @error('name') is-invalid @enderror"
                                            placeholder="Nama" name="name" value="{{ old('name', $organization->name) }}">
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputPhone">Nomor Hp</label>
                                        <input type="phone" class="form-control @error('phone') is-invalid @enderror"
                                            placeholder="nomor telepon" name="phone"
                                            value="{{ old('phone', $organization->phone) }}">
                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail">Email</label>
                                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                                            placeholder="Email" name="email" value="{{ old('email', $organization->email) }}">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail">Logo</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Upload</span>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file"
                                                    class="custom-file-input @error('logo') is-invalid @enderror"
                                                    id="inputGroupFile01" name="logo">
                                                <label class="custom-file-label" for="inputGroupFile01">Choose
                                                    file</label>
                                            </div>
                                            @error('logo')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputPhone">website</label>
                                        <input type="text" class="form-control @error('password') is-invalid @enderror"
                                            placeholder="website" name="website"
                                            value="{{ old('email', $organization->website) }}">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                 @if($action == 'create')
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputPhone">Manager</label>
                                       <select class="form-control @error('jenkel') is-invalid @enderror" name="manager_id">
                                          @foreach($manager as $data)
                                            <option value="{{ $data->id }}">
                                               {{ $data->name }}
                                            </option>
                                            @endforeach
                                        </select>
                                        @error('manager_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                  @endif
                                @if($action == 'edit')
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <img src="{{ !empty($organization->logo) ? $organization->logo : 'https://via.placeholder.com/150' }}"
                                            alt="" class="img-thumbnail" width="150">
                                        @error('logo')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>  
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary" name="button_submit">Simpan</button>
                                    <a href="" class="btn btn-danger">Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
<script>
    $(document).ready(function () {
        bsCustomFileInput.init();
    });
</script>

<script type="text/javascript">
    $('#customFile').change(function(){
        let file = $(this).val().split('.');
        let cn   = file.length;

        if(file[cn-1] != 'jpg' && file[cn-1] != 'png' && file[cn-1] != 'jpeg'){
            alert('type file upload tidak di ijinkan, type file harus .jpg/jpeg/png');
            $(this).val('');
        }
    });
</script>
@endsection
