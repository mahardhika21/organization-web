<form action="{{ route('organization.destroy', $organization->id) }}" method="post">
    @csrf
    @method('DELETE')
    <a href="{{ route('organization.edit', $organization->id) }}" class="btn btn-secondary btn-sm edit-data">
        Edit
    </a>
     <a href="{{ route('organization.show', $organization->id) }}" class="btn btn-info btn-sm edit-data">
        Detail
    </a>
    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Hapus data ini ?')">Hapus</button>
    
</form>