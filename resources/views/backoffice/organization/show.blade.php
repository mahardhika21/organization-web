@extends('backoffice.layouts.app')
@section('css')

@endsection
@section('content')
<div class="content-wrapper" style="min-height: 180px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $title }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Organizaation</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12 offset-md-0">
                    <!-- general form elements -->
                    <div class="card card-info">
                        <div class="card-header" style="background-color: #09aaaf; color: azure">
                            <h3 class="card-title">Data Organization</h3>
                        </div>
                        {{-- @if($errors->any())
                        {{ implode('', $errors->all('<div>:message</div>')) }}
                        @endif --}}
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body">
                            @if (session('message_success'))
                            <div class="alert alert-success">
                                {{ session('message_success') }}
                            </div>
                            @endif
                            @if (session('message_error'))
                            <div class="alert alert-warning">
                                {{ session('message_error') }}
                            </div>
                            @endif
                            <div class="card-body">
                                <div class="container">
                                    <div class="main-body">
                                        <div class="row gutters-sm">
                                            <div class="col-md-12">
                                                <div class="form-row">
                                                    <div class="form-group col-sm-3">
                                                        <div class="card mb-3">
                                                            <div class="card-body">
                                                                <div
                                                                    class="d-flex flex-column align-items-center text-center">
                                                                    <img height="158px;"
                                                                        src="{{ !empty($organization->logo) ? $organization->logo : 'https://www.nicepng.com/png/detail/52-521023_download-free-icon-female-vectors-blank-facebook-profile.png' }}"
                                                                        alt="Admin" class="rounded-circle" width="150">
                                                                    <div class="mt-3">
                                                                        @if($user->id ==  $organization->manager_id)
                                                                         <a style="cursor: pointer; color:white;"
                                                            class="btn btn-primary btn-block"
                                                            href="{{ route('organization.edit', $organization->id) }}">
                                                            Edit</a>
                                                            @endif
                                                                        <p class="text-secondary mb-1"></p>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-sm-8 offset-md-1">
                                                        <div class="card mb-3">
                                                            <div class="card-body">
                                                                <div class="d-flex flex-row-reverse">
                                                                    <h5 class="card-title"><b>Profile Organization</b></h5>
                                                                </div>
                                                                <hr>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                         <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <h6 class="mb-0">Nama</h6>
                                                                                </div>
                                                                                <div class="col-sm-1">:</div>
                                                                                <div class="col-sm-7 text-secondary">
                                                                                    {{ $organization->name }}
                                                                                </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                         <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <h6 class="mb-0">Email</h6>
                                                                                </div>
                                                                                <div class="col-sm-1">:</div>
                                                                                <div class="col-sm-7 text-secondary">
                                                                                    {{ $organization->email }}
                                                                                </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                         <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <h6 class="mb-0">Phone</h6>
                                                                                </div>
                                                                                <div class="col-sm-1">:</div>
                                                                                <div class="col-sm-7 text-secondary">
                                                                                    {{ $organization->phone }}
                                                                                </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                         <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <h6 class="mb-0">Website</h6>
                                                                                </div>
                                                                                <div class="col-sm-1">:</div>
                                                                                <div class="col-sm-7 text-secondary">
                                                                                    {{ $organization->email }}
                                                                                </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                         <div class="row">
                                                                                <div class="col-sm-4">
                                                                                    <h6 class="mb-0">Manager</h6>
                                                                                </div>
                                                                                <div class="col-sm-1">:</div>
                                                                                <div class="col-sm-7 text-secondary">
                                                                                    {{ $organization->manager->name }}
                                                                                </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                                <div class="card" id="">
                                                    <div class="d-flex flex-row-reverse m-2">
                                                        <h5 class="card-title">Data PIC</h5>
                                                    </div>
                                                    <hr>
                                                    <div class="card-body">
                                                         <div class="" style="margin: 12px;">
                                                            @if($user->id == $organization->manager_id)
                                                            <div class="d-flex flex-row-reverse m-2">
                                                            <h5 class="card-title"> <a style="cursor: pointer; color:white;"
                                                            class="btn btn-primary btn-block"
                                                            href="{{ route('users.create', 'type=pic&organization_id='. $organization->id) }}">
                                                            Tambah data Pic</a></h5>
                                                             </div>
                                                             @endif
                                                            <hr>
                                                            <table id="picData" class="table m-2">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">#</th>
                                                                        <th scope="col">Nama</th>
                                                                        <th scope="col">Phone</th>
                                                                        <th scope="col">Email</th>
                                                                        <th scope="col">Avatar</th>
                                                                        <th scope="col">Aksi</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


<!-- end modal menu -->


@endsection
@section('js')
<script src="{{ asset('storage/assets/backoffice/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('storage/assets/backoffice/js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $('.confirmation').on('click', function () {
        return confirm('Apakah Anda Yakin Untuk Menambah Data ini?');
    });
    let organization_id = '{{ $organization->id }}';

    $('#picData').DataTable({
            serverSide: true,
            processing: true,
            responsive: true,
            ajax: "{{ route('users.datatables') }}"+"?type=pic&organization_id="+organization_id,
            columns: [
            { name: 'id', searchable: false },
            { name: 'name', },
            { name: 'email', },
            { name: 'phone' },
            { name: 'avatar' },
            { name: 'actionPic', orderable: false, searchable: false }
            ],
            columnDefs : [
            {
                targets: 0,
                orderable: true,
                render: function ( data, type, row, meta ){
                return meta.row + meta.settings._iDisplayStart + 1;
            }
            },
            {
                targets : 4,
                orderable:true,
                render: function (data, type, row, meta) {
                 if( data == "" || data == null) {
                        return "<img src='https://via.placeholder.com/150'>";
                } else {
                    return "<img src='"+data+"' width='150'>";
                }
            }
            }
            ],
        });


</script>
@endsection
