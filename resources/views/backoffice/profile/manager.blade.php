@extends('backoffice.layouts.app')
@section('css')

@endsection
@section('content')
<div class="content-wrapper" style="min-height: 180px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $title }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Admin</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12 offset-md-0">
                    <!-- general form elements -->
                    <div class="card card-info">
                        <div class="card-header" style="background-color: #09aaaf; color: azure">
                            <h3 class="card-title">Data Klien</h3>
                        </div>
                        {{-- @if($errors->any())
                        {{ implode('', $errors->all('<div>:message</div>')) }}
                        @endif --}}
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body">
                            @if (session('message_success'))
                            <div class="alert alert-success">
                                {{ session('message_success') }}
                            </div>
                            @endif
                            @if (session('message_error'))
                            <div class="alert alert-warning">
                                {{ session('message_error') }}
                            </div>
                            @endif
                            <div class="card-body">
                                <div class="container">
                                    <div class="main-body">
                                        <div class="row gutters-sm">
                                            <div class="col-md-12">
                                                 <div class="form-row">
                                                    <div class="form-group col-sm-4">
                                                    <div class="card mb-3">
                                                    <div class="card-body">
                                                         <div class="d-flex flex-column align-items-center text-center">
                                                            <img height="158px;" src="{{ !empty($ibu->foto) ? $ibu->foto : 'https://www.nicepng.com/png/detail/52-521023_download-free-icon-female-vectors-blank-facebook-profile.png' }}"
                                                                alt="Admin" class="rounded-circle" width="150">
                                                            <div class="mt-3">
                                                                <h4>{{ $ibu->nama }}</h4>
                                                                <p class="text-secondary mb-1"></p>
                                                            </div>

                                                        </div>
                                                    </div>
                                                  </div>
                                                 </div>

                                                 <div class="form-group col-sm-8">
                                                    <div class="card mb-3">
                                                    <div class="card-body">
                                                        <div class="d-flex flex-row-reverse">
                                                            <h5 class="card-title"><b>Profile Utama</b></h5>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <h6 class="mb-0">Nama</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                {{ $ibu->nama }}
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <h6 class="mb-0">NIK</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                {{ $ibu->nik }}
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <h6 class="mb-0">Nomor Hp</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                {{ $ibu->no_hp }}
                                                            </div>
                                                        </div>
                                                        @if(!empty($ibu->klien_id))
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <h6 class="mb-0">Nomor Rm</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                {{ $ibu->klien->no_rm }}
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                 </div>
                                             </div>
                                             <div class="form-row">
                                                    <div class="form-group col-md-3">
                                                        <a style="cursor: pointer; color:white;"
                                                                class="btn btn-primary btn-block"
                                                                href="{{ route('backoffice.ibu.edit', $ibu->id) }}">Data Profile</a>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                      <a id="kehamilan"
                                                                style="cursor: pointer; color:white;"
                                                                class="btn btn-primary btn-block">Kehamilan</a>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                    <a id="persalinan"
                                                                style="cursor: pointer; color:white;"
                                                                class="btn btn-primary btn-block">Persalinan</a>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                     <a id="nifas"
                                                                style="cursor: pointer; color:white;"
                                                                class="btn btn-primary btn-block">Nifas</a>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <a id="kb"
                                                                style="cursor: pointer; color:white;"
                                                                class="btn btn-primary btn-block">Keluarga Berencana</a>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                       <a id="kespro"
                                                                style="cursor: pointer; color:white;"
                                                                class="btn btn-primary btn-block">Kesehatan Reproduksi</a>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                      <a id="kesmum"
                                                                style="cursor: pointer; color:white;"
                                                                class="btn btn-primary btn-block">Kesehatan Umum</a>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                     <a style="cursor: pointer; color:white;"
                                                                class="btn btn-warning btn-block"
                                                                onclick="window.history.back()">Kembali</a>
                                                    </div>

                                                </div>
                                                <div class="card mb-3" id="demografi_page">
                                                     <div class="d-flex flex-row-reverse m-2">
                                                            <h5 class="card-title">Profile Lengkap</h5>
                                                        </div>
                                                        <hr>
                                                    <div class="card-body">
                                                        <div class="form-row">
                                                            <div class="form-group col-md-6">
                                                                <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <h6 class="mb-0" style="">Nama Lengkap</h6>
                                                                    </div>
                                                                    <div class="col-sm-1">:</div>
                                                                    <div class="col-sm-7 text-secondary">
                                                                        {{ $ibu->nama_panjang }}
                                                                    </div>
                                                                 </div>
                                                                <hr>
                                                                 <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <h6 class="mb-0">Agama</h6>
                                                                    </div>
                                                                    <div class="col-sm-1">:</div>
                                                                    <div class="col-sm-7 text-secondary">
                                                                        {{ $ibu->agama }}
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                 <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <h6 class="mb-0">Pendidikan </h6>
                                                                    </div>
                                                                    <div class="col-sm-1">:</div>
                                                                    <div class="col-sm-7 text-secondary">
                                                                         {{ $ibu->pendidikan }}
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <h6 class="mb-0">Pekerjaan</h6>
                                                                    </div>
                                                                    <div class="col-sm-1">:</div>
                                                                    <div class="col-sm-7 text-secondary">
                                                                         {{ $ibu->pekerjaan }}
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <h6 class="mb-0">Status</h6>
                                                                    </div>
                                                                    <div class="col-sm-1">:</div>
                                                                    <div class="col-sm-7 text-secondary">
                                                                        {{ $ibu->status }}
                                                                    </div>
                                                                </div>
                                                                @if($ibu->status == 'menikah')
                                                                <hr>
                                                                <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <h6 class="mb-0">Suami</h6>
                                                                    </div>
                                                                    <div class="col-sm-1">:</div>
                                                                    <div class="col-sm-7 text-secondary">
                                                                         {{ !empty($ibu->suami_id) ? $ibu->suami->nama : 'Belum di di tambahkan' }}
                                                                    </div>
                                                                </div>
                                                                @endif
                                                                <hr>
                                                                <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <h6 class="mb-0">Alamat</h6>
                                                                    </div>
                                                                    <div class="col-sm-1">:</div>
                                                                    <div class="col-sm-7 text-secondary">
                                                                       {{ $ibu->alamat }}
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                 <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <h6 class="mb-0">Tanggal Lahir</h6>
                                                                    </div>
                                                                    <div class="col-sm-1">:</div>
                                                                    <div class="col-sm-7 text-secondary">
                                                                         {{ !empty($ibu->tanggal_lahir) ? date('m/d/Y',strtotime($ibu->tanggal_lahir)) : '-' }}
                                                                    </div>
                                                                 </div>
                                                                 <hr>
                                                                  <div class="row">
                                                                    <div class="col-sm-4">
                                                                                <h6 class="mb-0">Usia</h6>
                                                                     </div>
                                                                    <div class="col-sm-1">:</div>
                                                                    <div class="col-sm-7 text-secondary">
                                                                                @php
                                                                                $birthDate = new
                                                                                DateTime($ibu->tanggal_lahir);
                                                                                $today = new DateTime("today");
                                                                                if($birthDate > $today) {
                                                                                $usia = 0;
                                                                                } else {
                                                                                $usia = $today->diff($birthDate)->y;
                                                                                }
                                                                                @endphp
                                                                                {{ $usia .' Tahun' }}
                                                                            </div>
                                                                        </div>
                                                                      <hr>
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <h6 class="mb-0">Tempat Lahir</h6>
                                                                        </div>
                                                                        <div class="col-sm-1">:</div>
                                                                        <div class="col-sm-7 text-secondary">
                                                                            {{ $ibu->tempat_lahir }}
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <h6 class="mb-0">Provinsi</h6>
                                                                        </div>
                                                                        <div class="col-sm-1">:</div>
                                                                        <div class="col-sm-7 text-secondary">
                                                                            {{ !empty($ibu->desa) ?$ibu->desa->district->regency->provincie->name : '' }}
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <h6 class="mb-0">Kabupaten</h6>
                                                                        </div>
                                                                        <div class="col-sm-1">:</div>
                                                                        <div class="col-sm-7 text-secondary">
                                                                             {{ !empty($ibu->desa) ? $ibu->desa->district->regency->name : '' }}
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <h6 class="mb-0">Kecamatan</h6>
                                                                        </div>
                                                                        <div class="col-sm-1">:</div>
                                                                        <div class="col-sm-7 text-secondary">
                                                                             {{ !empty($ibu->desa) ? $ibu->desa->district->name : '' }}
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <h6 class="mb-0">Kelurahan / Desa</h6>
                                                                        </div>
                                                                        <div class="col-sm-1">:</div>
                                                                        <div class="col-sm-7 text-secondary">
                                                                            : {{ !empty($ibu->desa) ? $ibu->desa->name : '' }}
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        @if($status['anak'] == '1')
                                                        <div class="d-flex flex-row-reverse m-2">
                                                            <h5 class="card-title"><b>Data Anak</b></h5>
                                                        </div>
                                                        <hr>

                                                        <div class="table-responsive">
                                                            <a href="{{ route('backoffice.anak.create', 'ibu_id=' . $ibu->id) }}" class="btn btn-primary mb-2" title="tambah data anak"> <i class="fas fa-user-plus"></i></a>
                                                            <table class="table table-bordered table-hover" id="dataAnak">
                                                              <thead>
                                                                <tr>
                                                                  <th scope="col">#</th>
                                                                  <th scope="col">Anak</th>
                                                                  <th scope="col">Jenkel</th>
                                                                  <th scope="col">Tanggal lahir</th>
                                                                  <th scope="col">Aksi</th>
                                                                </tr>
                                                              </thead>
                                                            </table>
                                                        </div>
                                                        <hr>
                                                        @endif
                                                        @if($status['kehamilan'] == '1')
                                                        <div class="d-flex flex-row-reverse m-2">
                                                            <h5 class="card-title"><b>Data Kehamilan</b></h5>
                                                        </div>
                                                        <hr>
                                                        <div class="">
                                                           <table class="table table-bordered table-hover" id="kehamilanData">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">#</th>
                                                                        <th scope="col">Anak</th>
                                                                        <th scope="col">Usia Kehamilan Waktu Daftar</th>
                                                                        <th scope="col">Status</th>
                                                                        <th scope="col">Aksi</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                        <hr>
                                                        @endif
                                                        @if($status['persalinan'] == '1')
                                                        <div class="d-flex flex-row-reverse m-2">
                                                            <h5 class="card-title"><b>Data Persalinan</b></h5>
                                                        </div>
                                                        <hr>
                                                        <div class="">
                                                            <table class="table table-bordered table-hover" id="persalinanData">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">#</th>
                                                                        <th scope="col">Tanggal Persalinan</th>
                                                                        <th scope="col">Tipe</th>
                                                                        <th scope="col">Aksi</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                        <hr>
                                                        @endif
                                                        @if($status['nifas'] == '1')
                                                        <div class="d-flex flex-row-reverse m-2">
                                                            <h5 class="card-title"><b>Data Nifas</b></h5>
                                                        </div>
                                                        <hr>
                                                        <div class="">
                                                            <table class="table table-bordered table-hover" id="nifasData">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">#</th>
                                                                        <th scope="col">Tanggal</th>
                                                                        <th scope="col">Tipe</th>
                                                                        <th scope="col">Aksi</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                        <hr>
                                                        @endif
                                                        @if($status['kb'] == '1')
                                                        <div class="d-flex flex-row-reverse m-2">
                                                            <h5 class="card-title"><b>Data Kb</b></h5>
                                                        </div>
                                                        <hr>
                                                        <div class="">
                                                            <table class="table table-bordered table-hover" id="kbData">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">#</th>
                                                                        <th scope="col">Tanggal</th>
                                                                        <th scope="col">Metode</th>
                                                                        <th scope="col">Status</th>
                                                                        <th scope="col">Aksi</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                        @endif
                                                        <hr>
                                                        @if($status['kespro'] == '1')
                                                        <div class="d-flex flex-row-reverse m-2">
                                                            <h5 class="card-title"><b>Data Keshetan Produksi</b></h5>
                                                        </div>
                                                        <hr>
                                                        <div class="">
                                                            <table class="table table-bordered table-hover" id="kesproData">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">#</th>
                                                                        <th scope="col">Tanggal</th>
                                                                        <th scope="col">Paritas</th>
                                                                        <th scope="col">Aksi</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                        @endif
                                                        <hr>
                                                        @if($status['kesmum'] == '1')
                                                        <div class="d-flex flex-row-reverse m-2">
                                                            <h5 class="card-title"><b>Data Kb</b></h5>
                                                        </div>
                                                        <hr>
                                                        <div class="">
                                                            <table class="table table-bordered table-hover" id="kesmumData">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">#</th>
                                                                        <th scope="col">Tanggal</th>
                                                                        <th scope="col">Riwayat</th>
                                                                        <th scope="col">Aksi</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

<!-- modal menu -->
<div class="modal fade" id="modalklien" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titleModal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        {{-- <a class="btn btn-info" href="" id="listBtn">List Data <i class="fas fa-file"></i></a> --}}
         <a class="btn btn-info confirmation" href="" id="addbtn">Tambah Data</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- end modal menu -->

<!-- modal tambah anak -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Recipient:</label>
            <input type="text" class="form-control" id="recipient-name">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="message-text"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div>
    </div>
  </div>
</div>
<!-- end modal tambah anak -->
@endsection
@section('js')
<script src="{{ asset('storage/assets/backoffice/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('storage/assets/backoffice/js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    var ibuId = '{{ $ibu->id }}';
    var base_url = '{{ url("/") }}';
    var klienId = '{{ $ibu->klien_id }}';
    var kehamilan_status = '{{ $status["kehamilan"] }}';
    var persalinan_status = '{{ $status["persalinan"] }}';
    var nifas_status = '{{ $status["nifas"] }}';
    var anak_status = '{{ $status["anak"] }}';
    var kb_status = '{{ $status["kb"] }}';
    var kespro_status = '{{ $status["kespro"] }}';
    var kesmum_status = '{{ $status["kesmum"] }}';

    $('#kehamilan').on('click', function(){
       let title   = document.getElementById('titleModal');
       let listBtn = document.getElementById('listBtn');
       let addbtn  = document.getElementById('addbtn');
       title.innerHTML   = 'Kehamilan Klien';
       addbtn.innerHTML  = 'Tambah Data Kehamilan';

    // listBtn.href = window.location.origin+'/backoffice/kehamilan?ibu_id='+ibuId;
       addbtn.href  = '{{ route("backoffice.kehamilan.create", "ibu_id=". $ibu->id) }}';

        $('#modalklien').modal('show');
    });

    $('#persalinan').on('click', function(){
       let title   = document.getElementById('titleModal');
       let listBtn = document.getElementById('listBtn');
       let addbtn  = document.getElementById('addbtn');
       title.innerHTML   = 'Persalinan Klien';
       addbtn.innerHTML  = 'Tambah Data Persalinan';

    //    listBtn.href = window.location.origin+'/backoffice/persalinan?ibu_id='+ibuId;
       addbtn.href  = '{{ route("backoffice.persalinan.konfirmasi", "ibu_id=". $ibu->id) }}';

        $('#modalklien').modal('show');
    });

    $('#nifas').on('click', function(){
       let title   = document.getElementById('titleModal');
       let listBtn = document.getElementById('listBtn');
       let addbtn  = document.getElementById('addbtn');
       title.innerHTML   = 'Nifas Klien';
       addbtn.innerHTML  = 'Tambah Data Nifas';

       addbtn.href  = '{{ route("backoffice.nifas.konfirmasi", "ibu_id=". $ibu->id) }}';

        $('#modalklien').modal('show');
    });

    $('#kb').on('click', function(){
       let title   = document.getElementById('titleModal');
       let listBtn = document.getElementById('listBtn');
       let addbtn  = document.getElementById('addbtn');
       title.innerHTML   = 'Keluarga Berencana Klien (KB)';
       addbtn.innerHTML  = 'Tambah Data KB';

       addbtn.href  = '{{ route("backoffice.kb.create", "klien_id=". $ibu->klien_id) }}';

        $('#modalklien').modal('show');
    });

    $('#kespro').on('click', function(){

       let title   = document.getElementById('titleModal');
       let listBtn = document.getElementById('listBtn');
       let addbtn  = document.getElementById('addbtn');
       title.innerHTML   = 'Kesehatan Reproduksi';
       addbtn.innerHTML  = 'Tambah Data Kesehatan Reproduksi';

    //    listBtn.href = window.location.origin+'/backoffice/kespro?ibu_id='+ibuId;
       addbtn.href  = '{{ route("backoffice.kespro.create", "klien_id=". $ibu->klien_id) }}';

        $('#modalklien').modal('show');
    });

    $('#kesmum').on('click', function(){

        let title = document.getElementById('titleModal');
        let listBtn = document.getElementById('listBtn');
        let addbtn = document.getElementById('addbtn');
        title.innerHTML = 'Kesehatan Umum';
        addbtn.innerHTML = 'Tambah Data Kesehatan Umum';

        addbtn.href  = '{{ route("backoffice.kesmum.create", "klien_id=". $ibu->klien_id) }}';
        $('#modalklien').modal('show');
    });

    $('.confirmation').on('click', function () {
        return confirm('Apakah Anda Yakin Untuk Menambah Data ini?');
    });

    // get list data anak
    if(anak_status == '1') {
        $('#dataAnak').DataTable({
            serverSide: true,
            processing: true,
            responsive: true,
            ajax: "{{ route('backoffice.anak.datatables') }}" + '?ibu_id='+ibuId,
            columns: [
            { name: 'id', searchable: false },
            { name: 'nama', },
            { name: 'jenkel' },
            { name: 'tanggal_lahir' },
            { name: 'id', orderable: false, searchable: false }
            ],
            columnDefs : [
            {
                targets: 0,
                orderable: true,
                render: function ( data, type, row, meta ){
                return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                targets: 2,
                orderable: true,
                render: function ( data, type, row, meta ){
                    if(data == 'l') {
                        return 'Laki-Laki'
                    } else {
                        return 'Wanita';
                    }
                }
            },
            {
                targets  : 4,
                orderable:true,
                render: function (data, type, row, meta) {
                    return '<a class="btn btn-sm btn-info" href="'+ base_url +'/backoffice/anak/'+data+'">detail</a>';
                }
            }
            ],
        });
    }

    // get data kehamilan
    if(kehamilan_status == '1') {
        $('#kehamilanData').DataTable({
            serverSide: true,
            processing: true,
            responsive: true,
            ajax: "{{ route('backoffice.kehamilan.datatables') }}" + '?klien_id='+klienId,
            columns: [
            { name: 'id', searchable: false },
            { name: 'register', },
            { name: 'bulan' },
            { name: 'persalinan_status' },
            { name: 'id', orderable: false, searchable: false }
            ],
            columnDefs : [
            {
                targets: 0,
                orderable: true,
                render: function ( data, type, row, meta ){
                return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                targets: 2,
                orderable: true,
                render: function ( data, type, row, meta ){
                    return data + ' Bulan';
                }
            },
             {
                targets: 3,
                orderable: true,
                render: function ( data, type, row, meta ){
                   if(data == '1') {
                       return "Mengandung";
                   }else{
                       return "Selesai";
                   }
                }
            },
            {
                targets : 4,
                orderable:true,
                render: function (data, type, row, meta) {
                return '<a class="btn btn-sm btn-info" href="'+ base_url +'/backoffice/kehamilan/'+data+'">detail</a>';
            }
            }
        ],
        });
    }


 // get data kehamilan
   if(persalinan_status == '1') {
        $('#persalinanData').DataTable({
            serverSide: true,
            processing: true,
            responsive: true,
            ajax: "{{ route('backoffice.persalinan.datatables') }}" + '?klien_id='+klienId,
            columns: [
            { name: 'id', searchable: false },
            { name: 'tanggal_lahir', },
            { name: 'jenis' },
            { name: 'id', orderable: false, searchable: false }
            ],
            columnDefs : [
            {
                targets: 0,
                orderable: true,
                render: function ( data, type, row, meta ){
                return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                targets : 3,
                orderable:true,
                render: function (data, type, row, meta) {
                return '<a class="btn btn-sm btn-info" href="'+ base_url +'/backoffice/persalinan/'+data+'">detail</a>';
            }
            }
        ],
        });
   }

    if(nifas_status == '1') {
        $('#nifasData').DataTable({
            serverSide: true,
            processing: true,
            responsive: true,
            ajax: "{{ route('backoffice.nifas.datatables') }}" + '?klien_id='+klienId,
            columns: [
            { name: 'id', searchable: false },
            { name: 'register', },
            { name: 'type' },
            { name: 'action', orderable: false, searchable: false }
            ],
            columnDefs : [
            {
                targets: 0,
                orderable: true,
                render: function ( data, type, row, meta ){
                return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                targets:2,
                orderable: true,
                render: function (data, type, row, meta) {
                    if(data == 'persalinan') {
                        return 'Dengan Persalinan';
                    }else{
                        return 'Hanya Nifas';
                    }
                }
            },

            ],
        });
    }

    if(kb_status == '1') {
        $('#kbData').DataTable({
        serverSide: true,
        processing: true,
        responsive: true,
        ajax: "{{ route('backoffice.kb.datatables') }}" + '?klien_id='+klienId,
        columns: [
        { name: 'id', searchable: false },
        { name: 'register', },
        { name: 'metode' },
        { name: 'kb_stat' },
        { name: 'id', orderable: false, searchable: false }
        ],
        columnDefs : [
        {
            targets: 0,
            orderable: true,
            render: function ( data, type, row, meta ){
            return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        {
            targets: 3,
            orderable: true,
            render: function ( data, type, row, meta ){
                if(data == '1') {
                    return "Masi Berjalan";
                }else{
                    return "Berhenti";
                }
            }
        },
        {
            targets : 4,
            orderable:true,
            render: function (data, type, row, meta) {
            return '<a class="btn btn-sm btn-info" href="'+ base_url +'/backoffice/kb/'+data+'">detail</a>';
            }
        }
        ],
        });
    }

    if(kespro_status == '1') {
        $('#kesproData').DataTable({
            serverSide: true,
            processing: true,
            responsive: true,
            ajax: "{{ route('backoffice.kespro.datatables') }}" + '?klien_id='+klienId,
            columns: [
                { name: 'id', searchable: false },
                { name: 'tanggal_kunjungan', },
                { name: 'paritas' },
                { name: 'id', orderable: false, searchable: false }
            ],
        columnDefs : [
        {
            targets: 0,
            orderable: true,
            render: function ( data, type, row, meta ){
            return meta.row + meta.settings._iDisplayStart + 1;
            }
            },
            {
            targets : 3,
            orderable:true,
            render: function (data, type, row, meta) {
            return '<a class="btn btn-sm btn-info" href="'+ base_url +'/backoffice/kespro/'+data+'">detail</a>';
            }
            }
            ],
        });
    }

    if(kesmum_status == '1') {
        $('#kesmumData').DataTable({
            serverSide: true,
            processing: true,
            responsive: true,
            ajax: "{{ route('backoffice.kesmum.datatables') }}" + '?klien_id='+klienId,
            columns: [
            { name: 'id', searchable: false },
            { name: 'tanggal_kunjungan', },
            { name: 'riwayat_penyakit' },
            { name: 'id', orderable: false, searchable: false }
            ],
            columnDefs : [
            {
                targets: 0,
                orderable: true,
                render: function ( data, type, row, meta ){
                return meta.row + meta.settings._iDisplayStart + 1;
            }
            },
            {
                targets : 3,
                orderable:true,
                render: function (data, type, row, meta) {
                return '<a class="btn btn-sm btn-info" href="'+ base_url +'/backoffice/kesmum/'+data+'">detail</a>';
            }
            }
            ],
        });
    }

</script>
@endsection
