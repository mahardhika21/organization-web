<form action="{{ route('users.destroy', $user->id) }}" method="post">
    @csrf
    @method('DELETE')
    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Blocked data ini ?')">{{ $user->active == '1' ? 'Blocked Manager' : 'Unblock Manager' }}</button>
</form>