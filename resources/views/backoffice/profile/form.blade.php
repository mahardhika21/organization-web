@extends('backoffice.layouts.app')
@section('css')

@endsection
@section('content')
<div class="content-wrapper" style="min-height: 180px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $title }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Admin</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8 offset-md-2">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">{{ $action }} Admin</h3>
                        </div>
                        {{-- @if($errors->any())
                        {{ implode('', $errors->all('<div>:message</div>')) }}
                        @endif --}}
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="form-admin" method="POST"
                            action="{{ $action == 'create' ? route('backoffice.admin.store') : route('backoffice.admin.update', $admin->id) }}">
                            @csrf
                            @if($action == 'edit')
                            @method('PUT')
                            @endif
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="inputJudul">Nama</label>
                                    <input type="hidden" name="action" value="{{ $action }}">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                                        placeholder="nama admin" name="name" value="{{ old('name', $admin->name) }}">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputJudul">Email</label>
                                    <input type="text" class="form-control @error('email') is-invalid @enderror"
                                        placeholder="emial admin" name="email"
                                        value="{{ old('email', $admin->email) }}">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputJudul">Phone</label>
                                    <input type="text" class="form-control @error('phone') is-invalid @enderror"
                                        placeholder="Phone admin" name="phone"
                                        value="{{ old('phone', $admin->phone) }}">
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                
                                 <div class="form-group">
                                   <div class="input-group mt-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Upload</span>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file"
                                                    class="custom-file-input @error('photo') is-invalid @enderror"
                                                    id="inputGroupFile01" name="photo">
                                                <label class="custom-file-label" for="inputGroupFile01">Choose
                                                    file</label>
                                            </div>
                                            @error('photo')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputJudul">Password baru</label>
                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                        placeholder="passwor baru" name="password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputJudul">Ulangi Password Baru</label>
                                    <input type="password"
                                        class="form-control @error('password_confirmation') is-invalid @enderror"
                                        placeholder="konfirmasi password" name="password_confirmation" value="">
                                    @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button class="btn btn-default" onclick="window.history.back()">kembali</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection
@section('js')

@endsection
