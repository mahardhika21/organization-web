<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-color : #03a1a0">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link" style="background-color : #03a1a0">
        <img src="{{ url('storage/assets/backoffice/img/logo.png')  }}" alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Printerous</span>
    </a>
    <!-- Sidebar -->
    <div
        class="sidebar os-host os-theme-light os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition">
        <div class="os-resize-observer-host observed">
            <div class="os-resize-observer" style="left: 0px; right: auto;"></div>
        </div>
        <div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;">
            <div class="os-resize-observer"></div>
        </div>
        <div class="os-content-glue" style="margin: 0px -8px; width: 249px; height: 100px;"></div>
        <div class="os-padding">
            <div class="os-viewport os-viewport-native-scrollbars-invisible" style="overflow-y: scroll;">
                <div class="os-content" style="padding: 0px 8px; height: 100%; width: 100%;">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="{{ $user->avatar }}"
                                class="img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info">
                            <a href="#" class="d-block">{{ $user->role }}</a>
                        </div>
                    </div>

                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                            data-accordion="false">
                            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                            <li class="nav-item">
                                <a href="{{ url('/') }}" class="nav-link {{ $menu == 'home' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-home"></i>
                                    <p>
                                        Dashboard
                                        {{-- <i class="right fas fa-angle-left"></i> --}}
                                    </p>
                                </a>
                            </li>
                            <li class="nav-header">PENGATURAN</li>
                            <li class="nav-item {{ $menu == 'profile' ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link {{ $menu == 'profile' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-user"></i>
                                    <p>
                                        Profile
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{ route('profile.index') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Data Profile</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('profile.password') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Update Password</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        {{-- <li class="nav-header">MEMBER</li>
                           <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    Member
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('backoffice.member.index') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Member Aktif</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('backoffice.member.index') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Member Register</p>
                                    </a>
                                </li>
                            </ul>
                        </li> --}}
                         <li class="nav-header">DATA</li>
                            <li class="nav-item {{ $menu == 'struktur' ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-id-card-alt"></i>
                                    <p>
                                       Organization
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{ route('organization.index') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>List Organization</p>
                                        </a>
                                    </li>
                                    @if($user->role == 'admin')
                                     <li class="nav-item">
                                        <a href="{{ route('organization.create') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Daftar Organization</p>
                                        </a>
                                    </li>
                                    @endif
                                </ul>
                            </li>
                            @if($user->role == 'admin')
                                <li class="nav-item">
                                    <a href="{{ route('users.index', 'type=manager') }}" class="nav-link">
                                        <i class="nav-icon fas fa-users"></i>
                                        <p>Manager</p>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
            </div>
        </div>
        <div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable os-scrollbar-auto-hidden">
            <div class="os-scrollbar-track">
                <div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div>
            </div>
        </div>
        <div class="os-scrollbar os-scrollbar-vertical os-scrollbar-auto-hidden">
            <div class="os-scrollbar-track">
                <div class="os-scrollbar-handle" style="height: 11.7124%; transform: translate(0px, 0px);"></div>
            </div>
        </div>
        <div class="os-scrollbar-corner"></div>
    </div>
    <!-- /.sidebar -->
</aside>
