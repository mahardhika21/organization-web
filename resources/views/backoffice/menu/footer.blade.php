<!-- Main Footer -->
<footer class="main-footer">
    <strong>Copyright &copy; {{ date('Y') }}<a href="#"> Organization</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        {{-- <b>Version</b> 3.1.0-pre --}}
    </div>
</footer>
</div>
