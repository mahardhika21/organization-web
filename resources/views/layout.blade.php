<!DOCTYPE html>
<html lang="en">
<head>
<title>BAZNAS Sleman - Badan Amal Zakat Nasional Sleman</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Lingua project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="styles/scroll.css">
<link rel="stylesheet" type="text/css" href="styles/responsive.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<!--Google Font-->
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@700&display=swap" rel="stylesheet">
<!--Stylesheet-->
<link rel="stylesheet" href="js2/style.css">

<style>
	.top_header{
		font-family: 'Poppins', sans-serif; font-weight: bold; font-size: 15px; color: #005331; cursor: pointer;
	}
	.zakat{
		background-color: #ddd;
		border-radius: 5px;
		display: block;
		padding: 6px;
		text-align: center;
	}
	#carousell {
		margin: 15px;
		margin-top: 5%;
	}
	video {
		width: 100%;
		height: auto;
	}
	.video-container {
	position:relative;
	padding-bottom:56.25%;
	padding-top:30px;
	height:0;
	overflow:hidden;
}

.video-container iframe, .video-container object, .video-container embed {
	position:absolute;
	top:0;
	left:0;
	width:100%;
	height:100%;
}

.scrolling-wrapper {
  overflow-x: scroll;
  overflow-y: hidden;
  white-space: nowrap;
}

.card {
    display: inline-block;
  }

a.sliderImage {margin: 0 25px 0 25px;text-align: center;display: block;}
a.pname {margin: 25px;text-align: center;display: block;}
#slider {height:250px;}
body {background: #eee}

.text {
  display: block;
  width: 100px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}

.test {
	writing-mode: horizontal-tb;
}

.btx {
	color: blue;
}

</style>
</head>
<body>
    @yield('content')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="{{ asset('storage/backoffice/login/js/sPreloader.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <script src="{{ asset('storage/backoffice/login/js/main.js') }}"></script>
</body>

</html>
