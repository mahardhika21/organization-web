

## About Organization

website manajemen organization

## Instalasi

1. clone project at https://gitlab.com/mahardhika21/organization-web.git
2. composer install --no-scripts
3. buat database dengan nama printdb
4. setiing file .env
5. php artisan migrate:fresh --seed atau bisa import database printdb.sql
6. php artisan storage:link (pastikan folder storage sudah terhubung dengan folder public)
7. kunjungi alamat web local
   - email : admin@admin.com, passwrod : admn123


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
