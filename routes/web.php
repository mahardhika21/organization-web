<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('login', 'AuthController@index')->name('auth.login');
Route::post('login', 'AuthController@login')->name('atuh.login');

Route::group(['middleware' => ['role:all']], function () {
    Route::get('/', 'DashboardController@index');
    Route::get('logout', 'AuthController@logout')->name('auth.logout');
    Route::post('cari', 'DashboardController@cariData')->name('organization.cari');
    Route::get('info_bar', 'DashboardController@infoBar')->name('info_bar');
    Route::get('password', 'ProfileController@password')->name('profile.password');
    Route::put('password/update', 'ProfileController@updatePassword')->name('update.password');
    Route::resource('dashboard', 'DashboardController');
    Route::resource('profile', 'profileController');
    Route::get('organization/datatables', 'OrganizationController@datatables')->name('organization.datatables');
    Route::resource('organization', 'OrganizationController');
    Route::resource('manager', 'ManagerController');
    Route::group(['middleware' => ['role:admin,manager']], function () {
        Route::get('users/datatables', 'UsersController@datatables')->name('users.datatables');
        Route::resource('users', 'UsersController');
    });
});
