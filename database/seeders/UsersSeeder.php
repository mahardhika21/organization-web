<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name'     => 'admin',
            'email'    => 'admin@admin.com',
            'password' => bcrypt('admin123'),
            'role'     => 'admin',
            'avatar'   => 'https://adminlte.io/themes/v3/dist/img/user2-160x160.jpg',
        ]);

    }
}
