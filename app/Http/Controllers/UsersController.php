<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Organization;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Backoffice\ProfileRequest;
use App\Http\Services\UploadServices;
use Illuminate\Support\Facades\DB;
use Freshbitsweb\Laratables\Laratables;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        if($request->type == 'manager') {
            $title = 'List Manager';
            $view  = 'backoffice.manager.index';
            $menu = 'manager';
        } else {
            $title = 'List User';
            $view = 'backoffice.profile.index';
            $menu = 'manager';
        }
        return view($view, [
            "menu"   => $menu,
            "title"  => $title,
            "user"   => $user,
        ]);
    }

    public function datatables(Request $request)
    {
        return Laratables::recordsOf(User::class, function ($query) use ($request) {
            if ($request->type == 'manager') {
                return $query->where('role', 'manager');
            } elseif ($request->type == 'pic') {

                //echo $request->organization_id; die();
                return $query->where('role', 'pic')
                             ->where('organization_id', $request->organization_id);
            }
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->type == 'manager') {
            $title = 'Tambah Manager';
            $view  = 'backoffice.manager.form';
            $menu  = 'manager';
        } elseif ($request->type == 'pic') {
            $title = 'Tambah PIC(Person)';
            $view  = 'backoffice.pic.form';
            $menu  = 'pic';
        }
        return view($view, [
            "menu"        => $menu,
            "title"       => $title,
            "user"        => Auth::user(),
            "user_data"   => new User(),
            "action"      => "create",
            "type"        => $request->type,
            "organization_id" => $request->organization_id,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileRequest $request)
    {
        // echo $request->role; die();
        if($request->role == 'pic') {
            $organization = Organization::find($request->organization_id);
            if($organization->manager_id != Auth::user()->id) {
                return redirect()->back()->with('message_error', 'user no have access');
            }
        }
        DB::beginTransaction();
        try {
            $profile = new User();
            $file  = new UploadServices();
            $avatar = $file->uploadSingleFile($request->avatar, 'img/backoffice/profile');
            $profile->fill($request->all());
            $profile->password = bcrypt($request->password);
            if (!empty($avatar)) {
                $profile->avatar = $avatar;
            }
            $profile->save();
            DB::commit();
            if ($profile->role == 'pic') {
                return redirect()->route('organization.show', $profile->organization_id)->with('message_success', 'success store ' . $profile->role);
            } elseif ($profile->role == 'manager') {
                return redirect()->route('users.index', 'type=manager')->with('message_success', 'success store ');
            } else {
                return redirect('dashboard');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->back()->with('message_error', 'failed add user ' . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if(!empty($user)) {
            return redirect()->back()->with('message_error', 'user not found');
        }

        return view('backoffice.manager.show', [
            "menu"   => $user->role,
            "title"  => 'Data manager '. $user->name,
            "user"   => $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_data = User::find($id);
        if(empty($user_data)) {
            return redirect()->back();
        }
        if ($user_data->role == 'manager') {
            $title = 'Update Manager';
            $view  = 'backoffice.manager.form';
            $menu  = 'manager';
        } elseif ($user_data->role == 'pic') {
            $title = 'Update PIC(Person)';
            $view  = 'backoffice.pic.form';
            $menu  = 'pic';
        }
        return view($view, [
            "menu"        => $menu,
            "title"       => $title,
            "user"        => Auth::user(),
            "user_data"   => $user_data,
            "action"      => "edit",
            "type"        => $user_data->role,
            "organization_id" => $user_data->organization_id,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if ($user->role == 'pic') {
            $organization = Organization::find($user->organization_id);
            if ($organization->manager_id != Auth::user()->id) {
                return redirect()->back()->with('message_error', 'user no have access');
            }
        }
        DB::beginTransaction();
        try {
            $profile = $user;
            $file  = new UploadServices();
            $avatar = $file->uploadSingleFile($request->avatar, 'img/backoffice/profile');
            $profile->fill($request->all());
            $profile->password = bcrypt($request->password);
            if (!empty($avatar)) {
                $profile->avatar = $avatar;
            }
            $profile->save();
            DB::commit();
            if ($profile->role == 'pic') {
                return redirect()->route('organization.show', $profile->organization_id)->with('message_success', 'success store ' . $profile->role);
            } elseif ($profile->role == 'manager') {
                return redirect()->route('manager.index')->with('message_success', 'success store ');
            } else {
                return redirect('dashboard');
            }
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->back()->with('message_error', 'failed add user ' . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $requets, $id)
    {
        $user = Auth::user();
        $userData = User::find($id);
        if($user->role == 'admin') {
            $userData->active = $userData->active == '1' ? '0' : '1';
            $userData->update();
            $message = 'success bloked/unbloked manager';
        } elseif($user->role == 'manager') {
            if($userData->role == 'pic') {
                $message = 'success delete pic';
                $userData->delete();
            }
        }

        return redirect()->back()->with('message_success', $message);
    }
}
