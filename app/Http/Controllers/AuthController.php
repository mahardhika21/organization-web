<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            return redirect('dashboard');
        }
        return view('backoffice.auth.login', [
            'title' => 'Login Page',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
            return redirect('backoffice/dashboard');
        }
        return view('backoffice.auth.forget', [
            'title' => 'Lupa Password',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function login(Request $request)
    {
        $request->validate([
            "email" => 'required|string|email',
            'password' => 'required|string|min:5',
        ]);

        $credentials = [
            "email"    => $request->email,
            "password" => $request->password,
            "active"   => 1,
        ];

        if (Auth::attempt($credentials)) {
            return redirect('dashboard');
        } else {
            return redirect()->back()->with('error_message', 'login failed!, email, password dont exits or user not active');
        }
    }

    public function logout(Request $request)
    {

        Auth::logout();
        return redirect()->route('auth.login')->with('success_message', 'logout success !!');
    }
}
