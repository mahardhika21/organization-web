<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Organization;
use App\Models\User;
use App\Http\Requests\Backoffice\OrganizationGet;
use App\Http\Requests\Backoffice\OrganizationStore;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Support\Facades\DB;
use App\Http\Services\UploadServices;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        return view('backoffice.organization.index', [
            "title" => 'List organization',
            "menu"  => 'organization',
            "user"  => $user,
        ]);
    }

    public function datatables(Request $request)
    {
        return Laratables::recordsOf(Organization::class, function ($query) use ($request) {
            if($request->type == 'manager') {
                return $query->select('organization.*')
                        ->where('manager_id', $request->manager_id);
            } else {
                return $query->select('organization.*');
            }
            
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = Auth::user();
        if($user->role != 'admin') {
            return redirect()->back()->with('message_error', 'user not have access');
        }
        return view('backoffice.organization.form', [
            "title"            => "Tambah organization",
            "menu"             => "organization",
            "action"           => "create",
            "organization"     => new Organization(),
            "manager"          => User::where('role', 'manager')->get(),
            "user"  => $user,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrganizationStore $request)
    {
        DB::beginTransaction();
        try {
            $organization = new Organization();
            $file  = new UploadServices();
            $logo = $file->uploadSingleFile($request->logo, 'img/backoffice/logo');
            $organization->fill($request->all());
            $organization->user_id = Auth::user()->id;
            if (!empty($logo)) {
                $organization->logo = $logo;
            }
            $organization->save();
            DB::commit();
            return redirect()->route('organization.show', $organization->id)->with('message_success', 'success add organization');
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->back()->with('message_error', 'failed add organization ' . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('backoffice.organization.show', [
            "title"            => "Detail Organization",
            "menu"             => "organization",
            "action"           => "show",
            "organization"     => Organization::find($id),
            "user"             => Auth::user(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('backoffice.organization.form', [
            "title"            => "Edit organization",
            "menu"             => "organization",
            "action"           => "edit",
            "organization"     => Organization::find($id),
            "user"          => Auth::user()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrganizationStore $request, $id)
    {
        DB::beginTransaction();
        try {
            $organization = Organization::find($id);
            if($organization->manager_id != Auth::user()->id) {
                return redirect()->back()->with('message_error', 'user not have access');
            }
            $file  = new UploadServices();
            $logo = $file->uploadSingleFile($request->logo, 'img/backoffice/logo');
            $organization->fill($request->all());
            if (!empty($logo)) {
                $organization->logo = $logo;
            }
            $organization->update();
            DB::commit();
            return redirect()->route('organization.show', $organization->id)->with('message_success', 'success add organization ');
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->back()->with('message_error', 'failed add organization ' . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $organization = Organization::find($id);
        $user = Auth::user();
        if($user->role != 'admin') {
            if($organization->manager_id != $user->id) {
                return redirect()->back()->with('message_error', 'not have access ');
            }
        }
        $organization->active = '0';
        $organization->update();

        return redirect()->back()->with('message_success', 'success delete/deactive organization');
    }
}
