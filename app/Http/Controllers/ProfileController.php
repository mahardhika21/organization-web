<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Administrator;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Backoffice\ProfileRequest;
use App\Http\Services\UploadServices;
use App\Http\Requests\Backoffice\PasswordRequest;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backoffice.profile.index', [
            "menu"   => 'profile',
            "title"  => 'Profile User',
            "user"   => Auth::user(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function password()
    {
        $user = Auth::user();
        return view('backoffice.profile.password', [
            "menu"   => 'profile',
            "title"  => 'Update Password',
            "user_id" => $user->id,
            "user"    => $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileRequest $request, User $profile)
    {
        $file  = new UploadServices();
        $avatar = $file->uploadSingleFile($request->avatar, 'img/backoffice/profile');
        $profile->fill($request->all());
        $profile->password = bcrypt($request->password);
        if (!empty($avatar)) {
            $profile->avatar = $avatar;
        }
        $profile->save();

        return redirect()->back()->with('message_success', 'success update profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updatePassword(PasswordRequest $request, User $password)
    {
        $password->password = bcrypt($request->new_password);
        $password->update();

        return redirect()->back()->with('message_success', 'success update password');
    }
}
