<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        return view('backoffice.dashboard.index', [
            'title' => 'Dashboard Admin',
            'menu'  => 'home',
            'user'  => $user,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function infoBar()
    {
        return [
            "success" => "true",
            "message" => "success get data",
            "data"    => [
                "manager"  => DB::table('users')->where('role', 'manager')->count(),
                "organization" => DB::table('organization')->where('active', 1)->count(),
                "pic"   => DB::table('users')->where('role', 'pic')->count(),
            ],
        ];
    }

    public function cariData(Request $request)
    {
        if (empty($request->search)) {
            return redirect()->back()->with('message_error_data', 'data tidak ditemukan');
        }
        $data = DB::table('organization')->where(function ($query) use ($request) {
            return $query->where('name', 'like', '%' . $request->search . '%')
                ->orWhere('phone', 'like', '%' . $request->search . '%')
                ->orWhere('email', $request->search);
        })->first();

        if (empty($data)) {
            $data = DB::table('users')->where(function ($query) use ($request) {
                return $query->where('name', 'like', '%' . $request->search .'%')
                    ->orWhere('phone', 'like', '%' . $request->search . '%')
                    ->orWhere('email', $request->search);
            })->first();
            if (empty($data)) {
                return redirect()->back()->with('message_error_data', 'data klien tidak ditemukan');
            }

            return redirect()->route('organization.show', $data->organization_id);
        }

        return redirect()->route('organization.show', $data->id);
    }
}
