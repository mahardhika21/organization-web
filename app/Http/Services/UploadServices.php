<?php

namespace App\Http\Services;

class UploadServices
{
    public function uploadSingleFile($file, $path)
    {
        if (!is_null($file)) {
            $fname = rand(000, 999) . '-' . strtotime(date('Y-m-d H:i:s')) . '.' . $file->extension();
            if ($file->move('public/' . $path . '/', $fname)) {
                return url('/') . '/public/' . $path . '/' . $fname;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function uploadMultiFile($files, $path)
    {
        $data = [];
        if (is_array($files)) {
            foreach ($files as $file) {
                if (!is_null($file)) {
                    $fname = rand(000, 999) . '-' . strtotime(date('Y-m-d H:i:s')) . '.' . $file->extension();
                    if ($file->move('public/' . $path . '/', $fname)) {
                        array_push($data, url('/') . '/public/' . $path . '/' . $fname);
                    }
                }
            }
        }

        return $data;
    }

    public function deleteFile($tmpfile)
    {
        if(!empty($tmpfile)) {
            return unlink($tmpfile);
        }
    
    }
}
