<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\Request;

class PublicUpload
{
    function __construct()
    {
        //
    }

    public function uploadSingleFile($file, $type)
    {

        $folder = '/feedback-' . $type;

        //  $file     = $request->file($type);
        $fileName = rand(0000, 9999) . '-' . strtotime(date('Y-m-d H:i:s')) . '-' . $type . '-' . $file->getClientOriginalName();
        $s3       = \Storage::disk('s3');
        $filePath = $folder . '/' . $fileName;
        $upload   = $s3->put($filePath, file_get_contents($file), 'public');

        if ($upload) {
            $links = env('NEO_PUBLIC_LINKS') . '' . $filePath;
            return $links;
        } else {
            return null;
        }
    }

    public function uploadMulti($file, $type)
    {
        $folder = '/feedback-' . $type;

        //  $file     = $request->file($type);
        $fileName = rand(0000, 9999) . '-' . strtotime(date('Y-m-d H:i:s')) . '-' . $type . '-' . $file->getClientOriginalName();
        $s3       = \Storage::disk('s3');
        $filePath = $folder . '/' . $fileName;
        $upload   = $s3->put($filePath, file_get_contents($file), 'public');

        if ($upload) {
            $links = env('NEO_PUBLIC_LINKS') . '' . $filePath;
            return $links;
        } else {
            return null;
        }
    }
}
