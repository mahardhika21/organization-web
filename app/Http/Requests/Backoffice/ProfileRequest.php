<?php

namespace App\Http\Requests\Backoffice;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      //  echo $this->role; die();
      //  die();
        if($this->action == 'create') {
            if ($this->role == 'manager') {
                return [
                    "name"             => "required|min:3",
                    "email"            => "required|email|unique:users,email",
                    "phone"            => "required|min:9|unique:users,phone",
                    "avatar"           => "mimes:jpeg,jpg,png,gif|max:10000",
                    "password"         => "required|min:6|max:15",
                    "confirm_password" => "required|same:password",
                ];
            } else {
                return [
                    "name"             => "required|min:3",
                    "email"            => "required|email|unique:users,email",
                    "phone"            => "required|min:9|unique:users,phone",
                    "avatar"           => "mimes:jpeg,jpg,png,gif|max:10000",
                ];
            }
        } else {
            $user = Auth::user();
            return [
                "name"             => "required|min:3",
                "avatar"           => "mimes:jpeg,jpg,png,gif|max:10000",
            ];
        }
    }
}
