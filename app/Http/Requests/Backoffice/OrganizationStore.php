<?php

namespace App\Http\Requests\Backoffice;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class OrganizationStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->action == 'create') {
            return [
                "name"     => "required|min:3",
                "email"    => "required|email|unique:users,email,",
                "phone"    => "required|min:9|unique:users,phone,",
                "logo"    => "mimes:jpeg,jpg,png,gif|max:10000",
            ];
        } elseif($this->action == 'edit') {
            return [
                "name"     => "required|min:3",
                "email"    => "required|email|unique:users,email," . $this->id,
                "phone"    => "required|min:9|unique:users,phone," . $this->id,
                "logo"    => "mimes:jpeg,jpg,png,gif|max:10000",
            ];
        }
        
    }
}
