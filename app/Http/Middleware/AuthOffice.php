<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthOffice
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$roles)
    {
        if (!Auth::check()) {
            return redirect()->route('backoffice.auth.login');
        }

        if ($roles[0] == 'all') {
            return $next($request);
        }

        if (in_array(Auth::user()->role, $roles)) {
            return $next($request);
        } else {
            return redirect(env('BASE_CTRL'));
        }
    }
}
